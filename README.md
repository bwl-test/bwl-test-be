Este proyecto gestiona los recursos para poblar zonas horarias, clima, banderas de otros paises.
Y de la misma forma proporciona tareas pendientes y hechas.
Administra los usuarios en la plataforma.


# KnowledgeFe
Version Java 11

# Development server
Hay 2 formas de ejecutar el proyecto:

Usando los goals maven -> maven build, maven install y finalmente ejecute java - jar (indicando el jar de empaquetado que se generó en la carpeta de destino)
Importar el proyecto como "proyecto maven dentro de algún IDE (ECLIPSE, STS, Intellij)" 
1. Si tiene el complemento Spring, ejecute la aplicación Spring 
2. De lo contrario, seleccione la clase Java principal (Aplicación de servicio) y ejecútela como Aplicación Java.
La aplicación está configurada para ejecutarse en un tomcat y exponerse en el puerto 8080. 
Una vez que la aplicación esté ejecutándose, acceda a localhost:8080