package weatherapi;

public abstract class WeatherApiContants {


	public static final String URL_BASE = "http://api.weatherapi.com/";
	public static final String VERSION = "v1/";
	public static final String RESPONSE_FORMAT = "current.json";
	public static final String START_QUERY_PARAMS = "?";
	public static final String AMPERSAND = "&";
	public static final String KEY = "key=65ca539df5ac4e86bcd175500212308";
	public static final String QP_Q = "q=";
	public static final String LANGUAGE_TYPE = "lang=";
	public static final String LANGUAGE = "es";
	

	
}
