package com.bwl.Knowledge.test.be.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bwl.Knowledge.test.be.constants.DateConstants;
import com.bwl.Knowledge.test.be.dto.WatchDTO;

public class DateFunctions {

	public static String formatDate(Date currentDate, String formatDate) {
					
		String dateFormated = "";
		try {
			SimpleDateFormat formaterDate = new SimpleDateFormat(DateConstants.CURRENT_FORMAT_DATE); 
			Date dateFormat = formaterDate.parse(currentDate.toString());
						
			switch (formatDate) {
			
			case DateConstants.DATE_WITH_HOUR:
				
				dateFormated = new SimpleDateFormat(DateConstants.DATE_WITH_HOUR).format(dateFormat);
				
				break;
			case DateConstants.DATE_WITHOUT_HOUR:
				
				dateFormated = new SimpleDateFormat(DateConstants.DATE_WITHOUT_HOUR).format(dateFormat);
			
				break;
				
			case DateConstants.JUST_HOUR:
				dateFormated = new SimpleDateFormat(DateConstants.JUST_HOUR).format(dateFormat);
				break;
			default:
				
				return new SimpleDateFormat(DateConstants.DATE_WITHOUT_HOUR).format(dateFormat);
								
			}

		} catch (ParseException e) {

			e.printStackTrace();					
		} 
		
		
		return dateFormated;
				
	}
	
	
	
	public static String formatDate(String currentDate) {
		
		String hour = "";
		try {
			
			SimpleDateFormat formaterDate = new SimpleDateFormat(DateConstants.DATE_WITHOUT_SECONDS); 
			Date dateFormat = formaterDate.parse(currentDate);
			
			hour = new SimpleDateFormat(DateConstants.JUST_HOUR).format(dateFormat);

		} catch (ParseException e) {

			e.printStackTrace();					
		} 
		
		
		return hour;
				
	}
	
	public static WatchDTO buildWatchDTO(String watch) {
		
		String[] watchArr = watch.split(":");
		
		return WatchDTO.build(Integer.parseInt(watchArr[0]), Integer.parseInt(watchArr[1]), Integer.parseInt(watchArr[2]));
	}
	
	
}
