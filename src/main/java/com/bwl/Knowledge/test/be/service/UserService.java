package com.bwl.Knowledge.test.be.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.bwl.Knowledge.test.be.ctrl.form.CreationUserForm;
import com.bwl.Knowledge.test.be.ctrl.form.SignInForm;
import com.bwl.Knowledge.test.be.dto.CreateUserDTO;
import com.bwl.Knowledge.test.be.dto.SignInDTO;
import com.bwl.Knowledge.test.be.dto.UserDTO;
import com.bwl.Knowledge.test.be.entity.PersonalDetails;
import com.bwl.Knowledge.test.be.entity.User;
import com.bwl.Knowledge.test.be.exceptions.ConflictException;
import com.bwl.Knowledge.test.be.repository.PersonalDetailDAO;
import com.bwl.Knowledge.test.be.repository.UserDAO;
import com.bwl.Knowledge.test.be.utils.FunctionsName;

import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class UserService {
	
	private final UserDAO userDAO;
	private final PersonalDetailDAO personalDetailsDAO;
	
	public CreateUserDTO createUser(CreationUserForm creationUserForm) {
		String initials = "";
		User user;
		if(!creationUserForm.getPassword().equals(creationUserForm.getValidPassword()))
			throw new ConflictException("Las contraseñas no coinciden", "C005");
		
		 user = userDAO.findByEmail(creationUserForm.getEmail());
		 
		 if(user != null) 
			 throw new ConflictException("Correo ya registrado en otra cuenta", "C006");
		
		PersonalDetails personalDetails = personalDetailsDAO.save(new PersonalDetails(creationUserForm.getFullName()));
		
		if(personalDetails == null) 
			throw new ConflictException("Error al registrar detalles de usuario", "C007");
		
		initials = FunctionsName.getInitiales(creationUserForm.getFullName().split(" ")).toUpperCase();
		
		 user = userDAO.save(new User(creationUserForm, personalDetails, initials));

		if(user == null)
			throw new ConflictException("Error al registrar usuario", "C008");
		
		
		return CreateUserDTO.build(true, "Usuario creado con exito");
		
	}


	public SignInDTO signIn(SignInForm signInForm) {
		
		User user = userDAO.findByEmailAndPassword(signInForm.getEmail(), signInForm.getPassword());
				
		if(user == null)  
			throw new ConflictException("User or Password is not correct", "C001");
		
		user.updateLastLogin();
		
		userDAO.save(user);
				
		return SignInDTO.build(user);
		
	}
	
	public List<UserDTO> findAllUser() {
		
		List<User> userList = userDAO.findAll();
		
		return userList.stream().map(user -> UserDTO.build(user)).collect(Collectors.toList());
		
	}
	

}
