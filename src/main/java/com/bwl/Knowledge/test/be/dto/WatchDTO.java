package com.bwl.Knowledge.test.be.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class WatchDTO {
	
	private int hour;
	private int minutes;
	private int seconds;

	public static WatchDTO build(int hour, int minutes, int seconds) {
		
		return WatchDTO.builder()
				.hour(hour)
				.minutes(minutes)
				.seconds(seconds)
				.build();
	}

}
