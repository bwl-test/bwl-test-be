package com.bwl.Knowledge.test.be.projections;

import lombok.Data;

@Data
public class ConditionProjection {

    public String text;
    public String icon;
    public Integer code;
    
}
