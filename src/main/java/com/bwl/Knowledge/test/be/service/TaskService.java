package com.bwl.Knowledge.test.be.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.bwl.Knowledge.test.be.dto.TaskDTO;
import com.bwl.Knowledge.test.be.entity.Task;
import com.bwl.Knowledge.test.be.exceptions.ConflictException;
import com.bwl.Knowledge.test.be.repository.TaskDAO;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class TaskService {

	private final TaskDAO taskDAO;
	
	public List<TaskDTO> findAllTask() {
		
		List<Task> taskList = taskDAO.findAll();
		
		if(taskList == null) 
			throw new ConflictException("Error al recuperar las tareas", "C004");

		return taskList.stream().map(task -> TaskDTO.build(task)).collect(Collectors.toList());
	}
}
