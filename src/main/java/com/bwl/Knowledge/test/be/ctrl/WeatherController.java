package com.bwl.Knowledge.test.be.ctrl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bwl.Knowledge.test.be.dto.WeatherContryDTO;
import com.bwl.Knowledge.test.be.service.WeatherService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/weather")
public class WeatherController {
	
	private final WeatherService weatherService;
	
	@GetMapping() 
	public ResponseEntity<WeatherContryDTO> getWeatherContry(@RequestParam String country) {
		
		return ResponseEntity.ok(weatherService.currentWeather(country));
	}

}
