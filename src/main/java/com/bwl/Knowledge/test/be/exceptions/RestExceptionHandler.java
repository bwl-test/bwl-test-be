package com.bwl.Knowledge.test.be.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {
	
	@ExceptionHandler(ConflictException.class)
	public ResponseEntity<ResponseExceptionObject> conflictHandler(final ConflictException ex) {
		
		return new ResponseEntity<ResponseExceptionObject>(ResponseExceptionObject.build(ex.getMessage(), ex.getCode()),
				HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<ResponseExceptionObject> notFountHandler(final NotFoundException ex) {
		
		return new ResponseEntity<ResponseExceptionObject>(ResponseExceptionObject.build(ex.getMessage(), ex.getCode()),
				HttpStatus.NOT_FOUND);
	}

}
