package com.bwl.Knowledge.test.be.constants;

public abstract class DateConstants {
	
	public static final String DATE_WITH_HOUR = "dd/MM/yyyy HH:mm:ss";
	public static final String DATE_WITHOUT_HOUR = "dd/MM/yyyy";
	public static final String CURRENT_FORMAT_DATE = "yyyy-MM-dd HH:mm:ss.S";
	public static final String JUST_HOUR = "HH:mm:ss";
	public static final String DATE_WITHOUT_SECONDS = "yyyy-MM-dd HH:mm";

}
