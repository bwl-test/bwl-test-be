package com.bwl.Knowledge.test.be.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class BWLExceptions extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	private String message;
	private String code;
	
	public BWLExceptions() {
		
	}
	
	public BWLExceptions(String message, String code) {
		super();
		this.code = code;
		this.message = message;
	}
	
}
