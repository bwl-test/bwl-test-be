package com.bwl.Knowledge.test.be.ctrl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bwl.Knowledge.test.be.ctrl.form.CreationUserForm;
import com.bwl.Knowledge.test.be.ctrl.form.SignInForm;
import com.bwl.Knowledge.test.be.dto.CreateUserDTO;
import com.bwl.Knowledge.test.be.dto.SignInDTO;
import com.bwl.Knowledge.test.be.dto.UserDTO;
import com.bwl.Knowledge.test.be.service.UserService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/user")//users
public class UserController {
	
	private final UserService userService;
	
	//Agregar binding result
	@PostMapping
	public ResponseEntity<CreateUserDTO> createUser(@Valid @RequestBody CreationUserForm creationUserForm) {
		
		
		return ResponseEntity.ok(userService.createUser(creationUserForm));
	}
	
	@PostMapping("/auth")
	public ResponseEntity<SignInDTO> signIn(@Valid @RequestBody SignInForm signInForm) {
		
		
		return ResponseEntity.ok(userService.signIn(signInForm));
	}
	
	@GetMapping
	public ResponseEntity<List<UserDTO>> getAllUser() {
		
		return ResponseEntity.ok(userService.findAllUser());
	}
	

}
