package com.bwl.Knowledge.test.be.exceptions;

public class ConflictException extends BWLExceptions{

	private static final long serialVersionUID = 1L;
	
	public ConflictException() {
		
	}
	
	public ConflictException(String message, String code) {
		super(message, code);
	}
}
