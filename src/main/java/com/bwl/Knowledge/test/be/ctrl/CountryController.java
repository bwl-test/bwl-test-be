package com.bwl.Knowledge.test.be.ctrl;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bwl.Knowledge.test.be.dto.ContryDTO;
import com.bwl.Knowledge.test.be.service.ContryService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/country") 
public class CountryController {
	
	private final ContryService contryService;
	
	@GetMapping
	public ResponseEntity<List<ContryDTO>> getAllContries() {
		
		return ResponseEntity.ok(contryService.findAllContry());
	}

}
