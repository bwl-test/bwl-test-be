package com.bwl.Knowledge.test.be.projections;

import lombok.Data;

@Data
public class CurrentProjection {


    public Integer last_updated_epoch;
    public String last_updated;
    public Float temp_c;
    public Float temp_f;
    public Integer is_day;
    public ConditionProjection condition;
    public Float wind_mph;
    public Float wind_kph;
    public Integer wind_degree;
    public String wind_dir;
    public Float pressure_mb;
    public Float pressure_in;
    public Float precip_mm;
    public Float precip_in;
    public Integer humidity;
    public Integer cloud;
    public Float feelslike_c;
    public Float feelslike_f;
    public Float vis_km;
    public Float vis_miles;
    public Float uv;
    public Float gust_mph;
    public Float gust_kph;

}