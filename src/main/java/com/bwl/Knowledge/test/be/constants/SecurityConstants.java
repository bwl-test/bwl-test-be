package com.bwl.Knowledge.test.be.constants;

public class SecurityConstants {
	//abstract
	public static final String HTTP_GET_METHOD = "GET";
	public static final String HTTP_POST_METHOD = "POST";
	public static final String HTTP_PUT_METHOD = "PUT";
	public static final String HTTP_DELETE_METHOD = "DELETE";

}
