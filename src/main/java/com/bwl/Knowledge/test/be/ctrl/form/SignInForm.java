package com.bwl.Knowledge.test.be.ctrl.form;

import lombok.Data;

@Data
public class SignInForm {

	private String email;
	private String password;
}
