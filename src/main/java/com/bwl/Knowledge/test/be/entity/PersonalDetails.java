package com.bwl.Knowledge.test.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;

@Entity
@Getter
@Table(name = "personal_details")
public class PersonalDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@OneToOne(mappedBy = "personalDetails")
	private User user;

	public PersonalDetails(String name) {		
		this.name = name;		
	}
	
	public PersonalDetails() {}
	
	
}
