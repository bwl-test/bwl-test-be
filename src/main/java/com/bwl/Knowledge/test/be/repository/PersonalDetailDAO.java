package com.bwl.Knowledge.test.be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bwl.Knowledge.test.be.entity.PersonalDetails;

@Repository
public interface PersonalDetailDAO extends JpaRepository<PersonalDetails, Integer>{

}
