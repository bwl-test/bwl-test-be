package com.bwl.Knowledge.test.be.dto;

import com.bwl.Knowledge.test.be.constants.DateConstants;
import com.bwl.Knowledge.test.be.entity.User;
import com.bwl.Knowledge.test.be.utils.DateFunctions;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UserDTO {

	private String name;
	private String email;
	private String creationDate;
	private String lastLogin;
	
	public static UserDTO build(User user) {
						
		return UserDTO.builder()
				.name(user.getPersonalDetails().getName())
				.email(user.getEmail())
				.creationDate(DateFunctions.formatDate(user.getCreationDate(), DateConstants.DATE_WITHOUT_HOUR))
				.lastLogin(DateFunctions.formatDate(user.getLastLogin(), DateConstants.DATE_WITH_HOUR))
				.build();
	}
	

}
