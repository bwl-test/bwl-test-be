package com.bwl.Knowledge.test.be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bwl.Knowledge.test.be.entity.User;

@Repository
public interface UserDAO extends JpaRepository<User, Integer>{

	User findByEmailAndPassword(String email, String password);
	
	User findByEmail(String email);
}
