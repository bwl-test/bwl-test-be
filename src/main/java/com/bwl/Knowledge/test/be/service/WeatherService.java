package com.bwl.Knowledge.test.be.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import com.bwl.Knowledge.test.be.dto.WeatherContryDTO;
import com.bwl.Knowledge.test.be.projections.PatherWeatherProjection;
import com.bwl.Knowledge.test.be.utils.HttpClient;

import weatherapi.WeatherApiContants;

@Service
public class WeatherService {
	
	public WeatherContryDTO currentWeather(String contry) {

		String url = WeatherApiContants.URL_BASE
				.concat(WeatherApiContants.VERSION)
				.concat(WeatherApiContants.RESPONSE_FORMAT)
				.concat(WeatherApiContants.START_QUERY_PARAMS)
				.concat(WeatherApiContants.KEY)
				.concat(WeatherApiContants.AMPERSAND)
				.concat(WeatherApiContants.QP_Q)
				.concat(contry)
				.concat(WeatherApiContants.AMPERSAND)
				.concat(WeatherApiContants.LANGUAGE_TYPE)
				.concat(WeatherApiContants.LANGUAGE);		
		
		ResponseEntity<PatherWeatherProjection> result = HttpClient.httpGet(url, PatherWeatherProjection.class);
		
		return WeatherContryDTO.build(result.getBody());
		
	}
	
	
	
	

}
