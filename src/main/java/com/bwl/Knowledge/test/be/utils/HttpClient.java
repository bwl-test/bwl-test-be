package com.bwl.Knowledge.test.be.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public abstract class HttpClient {

	public static <T> ResponseEntity<T> httpGet(String url, Class<T> responseType) {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);			
		
		ResponseEntity<T> response = 
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, responseType);	
		
		return response;
	}
}

