package com.bwl.Knowledge.test.be.dto;

import com.bwl.Knowledge.test.be.entity.User;
import com.bwl.Knowledge.test.be.utils.FunctionsName;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SignInDTO {
	
	private String name;
	private String initials;
	private int id;

	
	public static SignInDTO build(User user) {
		return SignInDTO.builder()
		.name(FunctionsName.getITwoNames(user.getPersonalDetails().getName().split(" ")))
		.initials(user.getInitials())
		.id(user.getId())
		.build();
	}
}
