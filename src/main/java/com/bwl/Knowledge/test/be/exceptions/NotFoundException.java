package com.bwl.Knowledge.test.be.exceptions;

public class NotFoundException extends BWLExceptions{

	private static final long serialVersionUID = 1L;
	
	public NotFoundException() {}
	
	public NotFoundException(String message, String code) {		
		super(message, code);
	}
}
