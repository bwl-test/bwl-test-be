package com.bwl.Knowledge.test.be.dto;

import com.bwl.Knowledge.test.be.entity.Task;
import com.bwl.Knowledge.test.be.enums.TaskEnum;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TaskDTO {
	
	private String taskName;
	private int id;
	private TaskEnum status;
	
	public static TaskDTO build(Task task) {
		
		return TaskDTO.builder()
				.id(task.getId())
				.taskName(task.getTaskName())
				.status(task.getStatus())
				.build();
	}

}
