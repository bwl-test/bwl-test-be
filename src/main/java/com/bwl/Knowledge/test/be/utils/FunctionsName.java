package com.bwl.Knowledge.test.be.utils;

public class FunctionsName {
	
	public static String getInitiales(String[] split) {
		String initials = "";
		if(split.length > 3) {
			initials = split[1].substring(0, 1) + split[2].substring(0, 1);
		} else {
			initials = split[0].substring(0, 1) + split[1].substring(0, 1);
		}
		
		return initials;
	}
	
	public static String getITwoNames(String[] split) {
		String names = "";
		if(split.length > 3) {
			names = split[1].concat(" ").concat(split[2]);
		} else {
			names = split[0].concat(" ").concat(split[1]);
		}
		
		return names;
	}

}
