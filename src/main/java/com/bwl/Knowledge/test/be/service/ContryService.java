package com.bwl.Knowledge.test.be.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.bwl.Knowledge.test.be.dto.ContryDTO;
import com.bwl.Knowledge.test.be.entity.Country;
import com.bwl.Knowledge.test.be.repository.ContryDAO;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ContryService {
	
	private final ContryDAO contryDAO;
	
	public List<ContryDTO> findAllContry() {
		
		List<Country> contryList =  contryDAO.findAll();
		
		return contryList.stream().map(contry -> ContryDTO.build(contry)).collect(Collectors.toList());
	}
	

}
