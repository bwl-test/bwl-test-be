package com.bwl.Knowledge.test.be.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CreateUserDTO {
	
	boolean success;
	String message;
	
	public static CreateUserDTO build(boolean succes, String message) {
		
		return CreateUserDTO.builder()
				.message(message)
				.success(succes)
				.build();
	}
	
}
