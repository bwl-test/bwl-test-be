package com.bwl.Knowledge.test.be.projections;

import lombok.Data;

@Data
public class LocationProjection {
	
    public String name;
    public String region;
    public String country;
    public Float lat;
    public Float lon;
    public String tz_id;
    public Integer localtime_epoch;
    public String localtime;
    
}