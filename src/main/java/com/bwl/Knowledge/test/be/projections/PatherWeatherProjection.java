package com.bwl.Knowledge.test.be.projections;

import lombok.Data;

@Data
public class PatherWeatherProjection {
	
	private LocationProjection location;
	private CurrentProjection current;	
}
