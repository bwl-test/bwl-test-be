package com.bwl.Knowledge.test.be.exceptions;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseExceptionObject {
	
	private String message;
	private String code;
	
	public static ResponseExceptionObject build(final String message, final String code) {
		
		return ResponseExceptionObject.builder()
				.message(message)
				.code(code)
				.build();
	}

}
