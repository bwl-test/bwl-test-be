package com.bwl.Knowledge.test.be.utils;

import java.util.Optional;

import com.bwl.Knowledge.test.be.exceptions.ConflictException;

public class Assertions {
	
	public static void isOptionalNull(final Optional<?> object, final String id, final String entityName) {
		
		if(!object.isPresent()) {
			throw new ConflictException("Entity : ".concat(entityName).concat(" with ID ").concat("is not found"), "c100");
		}
		
	}
	
	public static void isNull(final Object object, final String entityName, final String id) {
		
		if(object == null) {
			throw new ConflictException("Entity ".concat(entityName).concat(" with ID ").concat(id).concat(" is not found "), "c100");
		}
	}

}
