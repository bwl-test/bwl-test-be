package com.bwl.Knowledge.test.be.ctrl.form;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CreationUserForm {

	//agregar mensajes
	@NotBlank
	private String email;
	
	@NotBlank
	private String password;
	
	@NotBlank
	private String validPassword;
	
	@NotBlank
	private String fullName;
	
}
