package com.bwl.Knowledge.test.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bwl.Knowledge.test.be.enums.TaskEnum;

import lombok.Getter;

@Entity
@Getter
@Table(name = "task")
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "task_name", nullable = false)
	private String taskName;
	
	@Column( name = "status", nullable = false)
	@Enumerated(value = EnumType.STRING)
	private TaskEnum status;
	
}
