package com.bwl.Knowledge.test.be.dto;

import com.bwl.Knowledge.test.be.entity.Country;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ContryDTO {

	private int id;
	private String name;
	private String queryParam;
	
	public static ContryDTO build(Country contry) {
		
		return ContryDTO.builder()
				.id(contry.getId())
				.name(contry.getName())
				.queryParam(contry.getQueryParam())
				.build();
	}
	
}
