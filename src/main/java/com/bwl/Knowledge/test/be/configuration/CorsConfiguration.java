package com.bwl.Knowledge.test.be.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.bwl.Knowledge.test.be.constants.SecurityConstants;


@Configuration
public class CorsConfiguration implements WebMvcConfigurer {
	
	@Override
	public void addCorsMappings(final CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("*").allowedMethods(SecurityConstants.HTTP_GET_METHOD,
				SecurityConstants.HTTP_POST_METHOD, SecurityConstants.HTTP_PUT_METHOD,
				SecurityConstants.HTTP_DELETE_METHOD);
	}
}
