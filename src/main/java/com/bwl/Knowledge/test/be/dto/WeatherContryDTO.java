package com.bwl.Knowledge.test.be.dto;

import com.bwl.Knowledge.test.be.projections.PatherWeatherProjection;
import com.bwl.Knowledge.test.be.utils.DateFunctions;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class WeatherContryDTO {

	private String region;
	private String country;
	private String weather;
	private String iconURI;
	private Float degrees;
	private WatchDTO time;
	
	public static WeatherContryDTO build(PatherWeatherProjection patherWeatherProjection) {
		
		DateFunctions.buildWatchDTO(DateFunctions.formatDate(patherWeatherProjection.getLocation().getLocaltime()));
		
		return WeatherContryDTO.builder()
				.region(patherWeatherProjection.getLocation().getRegion())
				.country(patherWeatherProjection.getLocation().getCountry())
				.weather(patherWeatherProjection.getCurrent().getCondition().getText())
				.iconURI(patherWeatherProjection.getCurrent().getCondition().getIcon())
				.degrees(patherWeatherProjection.getCurrent().getTemp_c())
				.time(
						DateFunctions.buildWatchDTO(
						DateFunctions.formatDate(patherWeatherProjection.getLocation().getLocaltime()) 
						))
				.build();
	}
	
}
