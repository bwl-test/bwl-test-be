package com.bwl.Knowledge.test.be.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.bwl.Knowledge.test.be.ctrl.form.CreationUserForm;

import lombok.Getter;

@Entity
@Getter
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id", nullable = false)
	private int id;
	
	@Column( name = "email", nullable = false)
	private String email;
	
	@Column( name = "password", nullable = false)
	private String password;
	
	@Column(name = "initials", nullable = false)
	private String initials;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column( name = "last_login", nullable = false)
	private Date lastLogin;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column( name = "creation_date", nullable = false)
	private Date creationDate;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "personal_details_fk")
	private PersonalDetails personalDetails;

	public User(CreationUserForm creationUserForm, PersonalDetails personalDetails, String initials) {
		
		this.email = creationUserForm.getEmail();
		this.password = creationUserForm.getPassword();
		this.initials = initials;
		this.lastLogin = new Date();
		this.creationDate = new Date();		
		this.personalDetails = personalDetails;
	
	}
	
	public User() {
		
	}
	
	public void updateLastLogin() {
		this.lastLogin = new Date();
	}
	
	
}
