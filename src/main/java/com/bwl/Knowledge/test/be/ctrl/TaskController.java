package com.bwl.Knowledge.test.be.ctrl;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bwl.Knowledge.test.be.dto.TaskDTO;
import com.bwl.Knowledge.test.be.service.TaskService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/task")
public class TaskController {
	
	private final TaskService taskService;

	@GetMapping
	public ResponseEntity<List<TaskDTO>> getAllTask() {
		
		return ResponseEntity.ok(taskService.findAllTask());
	}
}
